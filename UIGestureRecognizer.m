- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     
    UIView *tempView1 = [[[UIView alloc] initWithFrame:self.view.bounds] autorelease];
    [tempView1 setBackgroundColor:[UIColor blueColor]];
    [self.view addSubview:tempView1];
     
    UIView *tempView2 = [[[UIView alloc] initWithFrame:self.view.bounds] autorelease];
    [tempView2 setBackgroundColor:[UIColor orangeColor]];
    [self.view addSubview:tempView2];
     
    UITapGestureRecognizer *tapGestureTel1 = [[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickH:)]autorelease];
    UITapGestureRecognizer *tapGestureTel2 = [[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickH:)]autorelease];
    // Set required taps and number of touches
    //一个手指，拍击两次手势
    [tapGestureTel2 setNumberOfTapsRequired:2];
    [tapGestureTel2 setNumberOfTouchesRequired:1];
    //两个手指，拍击两次手势
    [tapGestureTel2 setNumberOfTapsRequired:2];
    [tapGestureTel2 setNumberOfTouchesRequired:2];
     
    [tempView1 addGestureRecognizer:tapGestureTel1];
    [tempView2 addGestureRecognizer:tapGestureTel2];
     
    // 向上滑动
    UISwipeGestureRecognizer *oneFingerSwipeUp =
    [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(clickH:)] autorelease];
    [oneFingerSwipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [tempView2 addGestureRecognizer:oneFingerSwipeUp];
    // 向下滑动
    UISwipeGestureRecognizer *oneFingerSwipeDown =
    [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(clickH:)] autorelease];
    [oneFingerSwipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
    [tempView2 addGestureRecognizer:oneFingerSwipeDown];
     
    //旋转手势
    UIRotationGestureRecognizer *twoFingersRotate =
    [[[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(clickH:)] autorelease];
    [tempView2 addGestureRecognizer:twoFingersRotate];
     
    //向里或向外捏的手势
    UIPinchGestureRecognizer *twoFingerPinch =
    [[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(clickH:)] autorelease];
    [tempView2 addGestureRecognizer:twoFingerPinch];
}
 
-(IBAction)clickH:(id)sender
{
    UIView *view = [[self.view subviews] objectAtIndex:1];
    [self.view insertSubview:view atIndex:0];
    //将当前的view放到最底部。
}